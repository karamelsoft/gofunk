package slices

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMap(t *testing.T) {
	double := func(value int) int {
		return value * 2
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		var expect []int
		assert.EqualValues(t, expect, Map(input, double))
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		assert.EqualValues(t, []int{2, 4, 6, 8}, Map(input, double))
	})
}

func TestFlatMap(t *testing.T) {
	expand := func(value int) []int {
		output := make([]int, 0, value)
		for i := 0; i < value; i++ {
			output = append(output, value)
		}
		return output
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		var expect []int
		assert.EqualValues(t, expect, FlatMap(input, expand))
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		assert.EqualValues(t, []int{
			1,
			2, 2,
			3, 3, 3,
			4, 4, 4, 4,
		}, FlatMap(input, expand))
	})
}

func TestFirst(t *testing.T) {
	even := func(value int) bool {
		return value%2 == 0
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		assert.EqualValues(t, 0, First(input, even))
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		assert.EqualValues(t, 2, First(input, even))
	})
}

func TestFirstOptional(t *testing.T) {
	even := func(value int) bool {
		return value%2 == 0
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		value, present := FirstOptional(input, even)
		assert.Equal(t, 0, value)
		assert.Equal(t, false, present)
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		value, present := FirstOptional(input, even)
		assert.Equal(t, 2, value)
		assert.Equal(t, true, present)
	})
}

func TestFirstUnsafe(t *testing.T) {
	fakeErr := fmt.Errorf("fake erre")
	even := func(value int) bool {
		return value%2 == 0
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		value, err := FirstUnsafe(input, even, fakeErr)
		assert.Equal(t, 0, value)
		assert.Equal(t, fakeErr, err)
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		value, err := FirstUnsafe(input, even, fakeErr)
		assert.Equal(t, 2, value)
		assert.Nil(t, err)
	})
}

func TestFilter(t *testing.T) {
	even := func(value int) bool {
		return value%2 == 0
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		var expect []int
		assert.EqualValues(t, expect, Filter(input, even))
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		expect := []int{2, 4}
		assert.EqualValues(t, expect, Filter(input, even))
	})
}

func TestFold(t *testing.T) {
	sum := func(a, b int) int {
		return a + b
	}
	t.Run("empty slice", func(t *testing.T) {
		var input []int
		assert.EqualValues(t, 0, Fold(input, 0, sum))
	})
	t.Run("non empty slice", func(t *testing.T) {
		input := []int{1, 2, 3, 4}
		assert.EqualValues(t, 10, Fold(input, 0, sum))
	})
}
