package slices

func Map[T, U any](source []T, mapping func(T) U) []U {
	var results []U
	for _, value := range source {
		results = append(results, mapping(value))
	}

	return results
}

func FlatMap[T, U any](source []T, mapping func(T) []U) []U {
	var results []U
	for _, value := range source {
		for _, uValue := range mapping(value) {
			results = append(results, uValue)
		}
	}

	return results
}

func Filter[T any](source []T, filter func(T) bool) []T {
	var results []T
	for _, value := range source {
		if filter(value) {
			results = append(results, value)
		}
	}

	return results
}

func Fold[T, U any](source []T, initial U, aggregation func(U, T) U) U {
	var result U = initial
	for _, value := range source {
		result = aggregation(result, value)
	}

	return result
}

func First[T any](source []T, filter func(T) bool) T {
	var result T
	for _, value := range source {
		if filter(value) {
			return value
		}
	}

	return result
}

func FirstOptional[T any](source []T, filter func(T) bool) (T, bool) {
	var result T
	for _, value := range source {
		if filter(value) {
			return value, true
		}
	}

	return result, false
}

func FirstUnsafe[T any](source []T, filter func(T) bool, err error) (T, error) {
	var result T
	for _, value := range source {
		if filter(value) {
			return value, nil
		}
	}

	return result, err
}
