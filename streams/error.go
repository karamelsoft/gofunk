package streams

func NewError[T any](value T, err error) Error[T] {
	return Error[T]{
		Value: value,
		Error: err,
	}
}

type Error[T any] struct {
	Value T
	Error error
}
