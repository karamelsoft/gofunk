package streams

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func testStream() Stream[int] {
	return Of(1, 2, 3, 4, 5)
}

func TestStream_Filter(t *testing.T) {
	assert.EqualValues(t,
		[]int{2, 4},
		testStream().
			Filter(func(value int) bool {
				return value%2 == 0
			}).
			Array(),
	)
}

func TestStream_FlatMap(t *testing.T) {
	assert.EqualValues(t,
		[]int{1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5},
		testStream().
			FlatMap(func(value int) Stream[int] {
				return Of(value, value, value)
			}).
			Array(),
	)
}

func Test_FlatMap(t *testing.T) {
	assert.EqualValues(t,
		[]string{"1", "1", "1", "2", "2", "2", "3", "3", "3", "4", "4", "4", "5", "5", "5"},
		FlatMap(testStream(), func(value int) Stream[string] {
			return Of(fmt.Sprintf("%d", value), fmt.Sprintf("%d", value), fmt.Sprintf("%d", value))
		}).Array(),
	)
}

func TestStream_FlatMapOptional(t *testing.T) {
	assert.EqualValues(t,
		[]int{2, 2, 2, 4, 4, 4},
		testStream().
			FlatMapOptional(func(value int) (Stream[int], bool) {
				return Of(value, value, value), value%2 == 0
			}).
			Array(),
	)
}

func Test_FlatMapOptional(t *testing.T) {
	assert.EqualValues(t,
		[]string{"2", "2", "2", "4", "4", "4"},
		FlatMapOptional(testStream(), func(value int) (Stream[string], bool) {
			return Of(fmt.Sprintf("%d", value), fmt.Sprintf("%d", value), fmt.Sprintf("%d", value)), value%2 == 0
		}).Array(),
	)
}

func TestStream_FlatMapUnsafe(t *testing.T) {
	result, errorChan := testStream().
		FlatMapUnsafe(func(value int) (Stream[int], error) {
			if value%2 == 0 {
				return Of(value, value, value), nil
			}
			return Of[int](), fmt.Errorf("fake error: %d", value)
		})
	var errors []Error[int]
	go func() {
		for err := range errorChan {
			errors = append(errors, err)
		}
	}()

	assert.EqualValues(t,
		[]int{2, 2, 2, 4, 4, 4},
		result.Array(),
	)
	assert.EqualValues(t,
		[]Error[int]{
			{Value: 1, Error: fmt.Errorf("fake error: %d", 1)},
			{Value: 3, Error: fmt.Errorf("fake error: %d", 3)},
			{Value: 5, Error: fmt.Errorf("fake error: %d", 5)},
		},
		errors,
	)
}

func Test_FlatMapUnsafe(t *testing.T) {
	result, errorChan := FlatMapUnsafe(
		testStream(),
		func(value int) (Stream[string], error) {
			if value%2 == 0 {
				return Of(fmt.Sprintf("%d", value), fmt.Sprintf("%d", value), fmt.Sprintf("%d", value)), nil
			}
			return Of[string](), fmt.Errorf("fake error: %d", value)
		})
	var errors []Error[int]
	go func() {
		for err := range errorChan {
			errors = append(errors, err)
		}
	}()

	assert.EqualValues(t,
		[]string{"2", "2", "2", "4", "4", "4"},
		result.Array(),
	)
	assert.EqualValues(t,
		[]Error[int]{
			{Value: 1, Error: fmt.Errorf("fake error: %d", 1)},
			{Value: 3, Error: fmt.Errorf("fake error: %d", 3)},
			{Value: 5, Error: fmt.Errorf("fake error: %d", 5)},
		},
		errors,
	)
}

func TestStream_First(t *testing.T) {
	assert.Equal(t,
		1,
		testStream().First(),
	)
	assert.Equal(t,
		0,
		Of[int]().First(),
	)
}

func TestStream_FirstOptional(t *testing.T) {
	first, present := testStream().FirstOptional()
	assert.Equal(t, 1, first)
	assert.True(t, present)

	first, present = Of[int]().FirstOptional()
	assert.Equal(t, 0, first)
	assert.False(t, present)
}

func TestStream_FirstUnsafe(t *testing.T) {
	first, err := testStream().FirstUnsafe(fmt.Errorf("fake error"))
	assert.NoError(t, err)
	assert.Equal(t, 1, first)

	first, err = Of[int]().FirstUnsafe(fmt.Errorf("fake error"))
	assert.Error(t, err)
	assert.Equal(t, 0, first)
}

func TestStream_Fold(t *testing.T) {
	assert.Equal(t,
		testStream().Fold(0, func(a int, b int) int {
			return a + b
		}),
		15,
	)
}

func Test_Fold(t *testing.T) {
	assert.Equal(t,
		Fold(testStream(), "", func(a string, b int) string {
			return fmt.Sprintf("%s%d", a, b)
		}),
		"12345",
	)
}

func TestStream_ForEach(t *testing.T) {
	var results []int

	testStream().ForEach(func(value int) {
		results = append(results, value)
	})

	assert.EqualValues(t, []int{1, 2, 3, 4, 5}, results)
}

func TestStream_Map(t *testing.T) {
	assert.EqualValues(t,
		[]int{2, 4, 6, 8, 10},
		testStream().Map(func(value int) int {
			return value * 2
		}).Array(),
	)
}

func Test_Map(t *testing.T) {
	assert.EqualValues(t,
		[]string{"2", "4", "6", "8", "10"},
		Map(testStream(), func(value int) string {
			return fmt.Sprintf("%d", value*2)
		}).Array(),
	)
}

func TestStream_MapOptional(t *testing.T) {
	assert.EqualValues(t,
		[]int{4, 8},
		testStream().MapOptional(func(value int) (int, bool) {
			return value * 2, value%2 == 0
		}).Array(),
	)
}

func Test_MapOptional(t *testing.T) {
	assert.EqualValues(t,
		[]string{"4", "8"},
		MapOptional(testStream(), func(value int) (string, bool) {
			return fmt.Sprintf("%d", value*2), value%2 == 0
		}).Array(),
	)
}

func TestStream_MapUnsafe(t *testing.T) {
	result, errorChan := testStream().MapUnsafe(func(value int) (int, error) {
		if value%2 == 0 {
			return value * 2, nil
		}
		return 0, fmt.Errorf("fake error: %d", value)
	})
	var errors []Error[int]
	go func() {
		for err := range errorChan {
			errors = append(errors, err)
		}
	}()

	assert.EqualValues(t,
		[]int{4, 8},
		result.Array(),
	)
	assert.EqualValues(t,
		[]Error[int]{
			{Value: 1, Error: fmt.Errorf("fake error: %d", 1)},
			{Value: 3, Error: fmt.Errorf("fake error: %d", 3)},
			{Value: 5, Error: fmt.Errorf("fake error: %d", 5)},
		},
		errors,
	)
}

func Test_MapUnsafe(t *testing.T) {
	result, errorChan := MapUnsafe(testStream(), func(value int) (string, error) {
		if value%2 == 0 {
			return fmt.Sprintf("%d", value*2), nil
		}
		return "0", fmt.Errorf("fake error: %d", value)
	})
	var errors []Error[int]
	go func() {
		for err := range errorChan {
			errors = append(errors, err)
		}
	}()

	assert.EqualValues(t,
		[]string{"4", "8"},
		result.Array(),
	)
	assert.EqualValues(t,
		[]Error[int]{
			{Value: 1, Error: fmt.Errorf("fake error: %d", 1)},
			{Value: 3, Error: fmt.Errorf("fake error: %d", 3)},
			{Value: 5, Error: fmt.Errorf("fake error: %d", 5)},
		},
		errors,
	)
}
