package streams

import "sync"

func Processing[T any](source <-chan T, handler func(T)) *ChanProcessing[T] {
	return &ChanProcessing[T]{
		source:  source,
		handler: handler,
	}
}

type ChanProcessing[T any] struct {
	source  <-chan T
	handler func(T)
}

func (c *ChanProcessing[T]) process(work *sync.WaitGroup) {
	defer work.Done()
	for value := range c.source {
		c.handler(value)
	}
}

func WaitForMany[T any](processings ...*ChanProcessing[T]) {
	work := &sync.WaitGroup{}
	work.Add(len(processings))
	for _, processing := range processings {
		go processing.process(work)
	}
	work.Wait()
}

func WaitForBoth[T any, U any](processing1 *ChanProcessing[T], processing2 *ChanProcessing[U]) {
	work := &sync.WaitGroup{}
	work.Add(2)
	go processing1.process(work)
	go processing2.process(work)
	work.Wait()
}
