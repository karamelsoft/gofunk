package streams

import (
	"gitlab.com/karamelsoft/gofunk/funs"
	"gitlab.com/karamelsoft/gofunk/maps"
)

func FromChan[T any](ch <-chan T) Stream[T] {
	return Stream[T]{
		ch: ch,
	}
}

func FromArray[T any](values []T) Stream[T] {
	return Of(values...)
}

func FromMapKeys[K comparable, T any](keys map[K]T) Stream[K] {
	return FromArray(maps.Keys(keys))
}

func FromMapValues[K comparable, T any](keys map[K]T) Stream[T] {
	return FromArray(maps.Values(keys))
}

func Of[T any](values ...T) Stream[T] {
	ch := make(chan T)
	go func() {
		defer close(ch)
		for _, value := range values {
			ch <- value
		}
	}()

	return FromChan[T](ch)
}

type Stream[T any] struct {
	ch <-chan T
}

func (s Stream[T]) ForEach(function func(T)) {
	for value := range s.ch {
		function(value)
	}
}

func Map[T any, U any](stream Stream[T], mapping func(T) U) Stream[U] {
	newChan := make(chan U)

	go func() {
		defer close(newChan)
		stream.ForEach(func(value T) {
			newChan <- mapping(value)
		})
	}()

	return FromChan[U](newChan)
}

func (s Stream[T]) Map(mapping func(T) T) Stream[T] {
	newChan := make(chan T)

	go func() {
		defer close(newChan)
		s.ForEach(func(value T) {
			newChan <- mapping(value)
		})
	}()

	return FromChan[T](newChan)
}

func MapOptional[T any, U any](stream Stream[T], mapping func(T) (U, bool)) Stream[U] {
	newChan := make(chan U)

	go func() {
		defer close(newChan)
		stream.ForEach(func(value T) {
			if newValue, present := mapping(value); present {
				newChan <- newValue
			}
		})
	}()

	return FromChan[U](newChan)
}

func (s Stream[T]) MapOptional(mapping func(T) (T, bool)) Stream[T] {
	newChan := make(chan T)

	go func() {
		defer close(newChan)
		s.ForEach(func(value T) {
			if newValue, present := mapping(value); present {
				newChan <- newValue
			}
		})
	}()

	return FromChan[T](newChan)
}

func MapUnsafe[T any, U any](stream Stream[T], mapping func(T) (U, error)) (Stream[U], <-chan Error[T]) {
	values := make(chan U)
	errors := make(chan Error[T])

	go func() {
		defer close(values)
		defer close(errors)
		stream.ForEach(func(value T) {
			newValue, err := mapping(value)
			if err != nil {
				errors <- NewError[T](value, err)
			} else {
				values <- newValue
			}
		})
	}()

	return FromChan[U](values), errors
}

func (s Stream[T]) MapUnsafe(mapping func(T) (T, error)) (Stream[T], <-chan Error[T]) {
	values := make(chan T)
	errors := make(chan Error[T])

	go func() {
		defer close(values)
		defer close(errors)
		s.ForEach(func(value T) {
			newValue, err := mapping(value)
			if err != nil {
				errors <- NewError[T](value, err)
			} else {
				values <- newValue
			}
		})
	}()

	return FromChan[T](values), errors
}

func FlatMap[T any, U any](stream Stream[T], mapping func(T) Stream[U]) Stream[U] {
	values := make(chan U)

	go func() {
		defer close(values)
		stream.ForEach(func(value T) {
			mapping(value).ForEach(funs.SendTo[U](values))
		})
	}()

	return FromChan[U](values)
}

func (s Stream[T]) FlatMap(mapping func(T) Stream[T]) Stream[T] {
	values := make(chan T)

	go func() {
		defer close(values)
		s.ForEach(func(value T) {
			mapping(value).ForEach(funs.SendTo[T](values))
		})
	}()

	return FromChan[T](values)
}

func FlatMapOptional[T any, U any](stream Stream[T], mapping func(T) (Stream[U], bool)) Stream[U] {
	values := make(chan U)

	go func() {
		defer close(values)
		stream.ForEach(func(value T) {
			if newValues, present := mapping(value); present {
				newValues.ForEach(funs.SendTo[U](values))
			}
		})
	}()

	return FromChan[U](values)
}

func (s Stream[T]) FlatMapOptional(mapping func(T) (Stream[T], bool)) Stream[T] {
	values := make(chan T)

	go func() {
		defer close(values)
		s.ForEach(func(value T) {
			if newValues, present := mapping(value); present {
				newValues.ForEach(funs.SendTo[T](values))
			}
		})
	}()

	return FromChan[T](values)
}

func FlatMapUnsafe[T any, U any](stream Stream[T], mapping func(T) (Stream[U], error)) (Stream[U], <-chan Error[T]) {
	values := make(chan U)
	errors := make(chan Error[T])

	go func() {
		defer close(values)
		defer close(errors)
		stream.ForEach(func(value T) {
			newValues, err := mapping(value)
			if err != nil {
				errors <- NewError[T](value, err)
			} else {
				newValues.ForEach(funs.SendTo[U](values))
			}
		})
	}()

	return FromChan[U](values), errors
}

func (s Stream[T]) FlatMapUnsafe(mapping func(T) (Stream[T], error)) (Stream[T], <-chan Error[T]) {
	values := make(chan T)
	errors := make(chan Error[T])

	go func() {
		defer close(values)
		defer close(errors)
		s.ForEach(func(value T) {
			newValues, err := mapping(value)
			if err != nil {
				errors <- NewError[T](value, err)
			} else {
				newValues.ForEach(funs.SendTo[T](values))
			}
		})
	}()

	return FromChan[T](values), errors
}

func (s Stream[T]) Filter(filter func(T) bool) Stream[T] {
	values := make(chan T)

	go func() {
		defer close(values)
		s.ForEach(func(value T) {
			if filter(value) {
				values <- value
			}
		})
	}()

	return FromChan[T](values)
}

func (s Stream[T]) First() T {
	return <-s.ch
}

func (s Stream[T]) FirstOptional() (T, bool) {
	value, present := <-s.ch
	return value, present
}

func (s Stream[T]) FirstUnsafe(err error) (T, error) {
	value, present := <-s.ch
	if present {
		return value, nil
	}

	return value, err
}

func Fold[T any, U any](stream Stream[T], initial U, aggregation func(U, T) U) U {
	result := initial
	stream.ForEach(func(value T) {
		result = aggregation(result, value)
	})

	return result
}

func (s Stream[T]) Fold(initial T, aggregation func(T, T) T) T {
	result := initial
	s.ForEach(func(value T) {
		result = aggregation(result, value)
	})

	return result
}

func (s Stream[T]) Chan() <-chan T {
	return s.ch
}

func (s Stream[T]) Array() []T {
	var values []T
	s.ForEach(funs.Append(&values))

	return values
}
