package funs

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestAppend(t *testing.T) {
	work := &sync.WaitGroup{}
	work.Add(2)

	var odds []int
	var evens []int
	oddsChan, evensChan := oddsAndEven()
	go func() {
		defer work.Done()
		for odd := range oddsChan {
			Append(&odds)(odd)
		}
	}()
	go func() {
		defer work.Done()
		for even := range evensChan {
			Append(&evens)(even)
		}
	}()

	work.Wait()
	assert.EqualValues(t, []int{1, 3}, odds)
	assert.EqualValues(t, []int{0, 2, 4}, evens)
}

func TestIgnore(t *testing.T) {
	work := &sync.WaitGroup{}
	work.Add(2)

	var odds []int
	oddsChan, evensChan := oddsAndEven()
	go func() {
		defer work.Done()
		for odd := range oddsChan {
			Append(&odds)(odd)
		}
	}()
	go func() {
		defer work.Done()
		for even := range evensChan {
			Ignore[int]()(even)
		}
	}()

	work.Wait()
	assert.EqualValues(t, []int{1, 3}, odds)
}

func oddsAndEven() (<-chan int, <-chan int) {
	odds := make(chan int)
	evens := make(chan int)

	go func() {
		defer close(odds)
		defer close(evens)
		for i := 0; i < 5; i++ {
			if i%2 == 0 {
				evens <- i
			} else {
				odds <- i
			}
		}
	}()

	return odds, evens
}
