package funs

func Append[T any](values *[]T) func(T) {
	return func(value T) {
		*values = append(*values, value)
	}
}

func Ignore[T any]() func(T) {
	return func(_ T) {}
}

func SendTo[T any](ch chan T) func(T) {
	return func(value T) {
		ch <- value
	}
}
