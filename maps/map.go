package maps

func Map[K, L comparable, T, U any](input map[K]T, mapping func(K, T) (L, U)) map[L]U {
	output := make(map[L]U)
	for key, value := range input {
		newKey, newValue := mapping(key, value)
		output[newKey] = newValue
	}

	return output
}

func MapKeys[K, L comparable, T any](input map[K]T, mapping func(K) L) map[L]T {
	return Map[K, L, T, T](input, func(key K, value T) (L, T) {
		return mapping(key), value
	})
}

func MapValues[K comparable, T, U any](input map[K]T, mapping func(T) U) map[K]U {
	return Map[K, K, T, U](input, func(key K, value T) (K, U) {
		return key, mapping(value)
	})
}

func Keys[K comparable, T any](input map[K]T) []K {
	output := make([]K, 0, len(input))
	for key, _ := range input {
		output = append(output, key)
	}

	return output
}

func Values[K comparable, T any](input map[K]T) []T {
	output := make([]T, 0, len(input))
	for _, value := range input {
		output = append(output, value)
	}

	return output
}

func MappedKeys[K comparable, T any, I any](input map[K]T, mapping func(K) I) []I {
	output := make([]I, 0, len(input))
	for key, _ := range input {
		output = append(output, mapping(key))
	}

	return output
}

func MappedValues[K comparable, T any, I any](input map[K]T, mapping func(T) I) []I {
	output := make([]I, 0, len(input))
	for _, value := range input {
		output = append(output, mapping(value))
	}

	return output
}
