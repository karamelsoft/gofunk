package maps

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"sort"
	"testing"
)

func TestKeys(t *testing.T) {
	t.Run("empty map", func(t *testing.T) {
		input := map[string]int{}
		assert.EqualValues(t, []string{}, Keys(input))
	})
	t.Run("non empty map", func(t *testing.T) {
		input := map[string]int{
			"one": 1,
			"two": 2,
		}
		output := Keys(input)
		sort.Strings(output)
		assert.EqualValues(t, []string{"one", "two"}, output)
	})
}

func TestValues(t *testing.T) {
	t.Run("empty map", func(t *testing.T) {
		input := map[string]int{}
		assert.EqualValues(t, []int{}, Values(input))
	})
	t.Run("non empty map", func(t *testing.T) {
		input := map[string]int{
			"one": 1,
			"two": 2,
		}
		output := Values(input)
		sort.Ints(output)
		assert.EqualValues(t, []int{1, 2}, output)
	})
}

func TestMap(t *testing.T) {
	mapping := func(key string, value int) (int, string) {
		return value, key
	}

	t.Run("empty map", func(t *testing.T) {
		input := map[string]int{}
		assert.EqualValues(t, map[int]string{}, Map(input, mapping))
	})
	t.Run("non empty map", func(t *testing.T) {
		input := map[string]int{
			"one": 1,
			"two": 2,
		}
		assert.EqualValues(t, map[int]string{
			1: "one",
			2: "two",
		}, Map(input, mapping))
	})
}

func TestMapKeys(t *testing.T) {
	mapping := func(key string) float32 {
		switch key {
		case "one":
			return 1.0
		case "two":
			return 2.0
		default:
			return -1.0
		}
	}

	t.Run("empty map", func(t *testing.T) {
		input := map[string]int{}
		assert.EqualValues(t, map[float32]int{}, MapKeys(input, mapping))
	})
	t.Run("non empty map", func(t *testing.T) {
		input := map[string]int{
			"one": 1,
			"two": 2,
		}
		assert.EqualValues(t, map[float32]int{
			1.0: 1,
			2.0: 2,
		}, MapKeys(input, mapping))
	})
}

func TestMapValues(t *testing.T) {
	mapping := func(value int) string {
		return fmt.Sprintf("%d", value)
	}

	t.Run("empty map", func(t *testing.T) {
		input := map[string]int{}
		assert.EqualValues(t, map[string]string{}, MapValues(input, mapping))
	})
	t.Run("non empty map", func(t *testing.T) {
		input := map[string]int{
			"one": 1,
			"two": 2,
		}
		assert.EqualValues(t, map[string]string{
			"one": "1",
			"two": "2",
		}, MapValues(input, mapping))
	})

}
