package monad

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestMap(t *testing.T) {
	toString := func(value int) string {
		return fmt.Sprint(value)
	}

	tests := []struct {
		name    string
		initial Result[int]
		expect  Result[string]
	}{
		{name: "error", initial: Error[int](fakeError), expect: Error[string](fakeError)},
		{name: "empty", initial: Empty[int](), expect: Empty[string]()},
		{name: "value", initial: Value(42), expect: Value("42")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, MapResult(tt.initial, toString))
		})
	}
}

func TestMapOptional(t *testing.T) {
	toString := func(present bool) func(value int) (string, bool) {
		return func(value int) (string, bool) {
			return fmt.Sprint(value), present
		}
	}

	tests := []struct {
		name    string
		initial Result[int]
		mapping func(value int) (string, bool)
		expect  Result[string]
	}{
		{name: "error not present", initial: Error[int](fakeError), mapping: toString(false), expect: Error[string](fakeError)},
		{name: "error present", initial: Error[int](fakeError), mapping: toString(true), expect: Error[string](fakeError)},
		{name: "empty not present", initial: Empty[int](), mapping: toString(false), expect: Empty[string]()},
		{name: "empty present", initial: Empty[int](), mapping: toString(true), expect: Empty[string]()},
		{name: "value not present", initial: Value(42), mapping: toString(false), expect: Empty[string]()},
		{name: "value present", initial: Value(42), mapping: toString(true), expect: Value("42")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := MapResultOptional(tt.initial, tt.mapping); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("MapResultOptional() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestMapUnsafe(t *testing.T) {
	toString := func(err error) func(value int) (string, error) {
		return func(value int) (string, error) {
			if err != nil {
				return "", err
			}
			return fmt.Sprint(value), nil
		}
	}

	tests := []struct {
		name    string
		initial Result[int]
		mapping func(value int) (string, error)
		expect  Result[string]
	}{
		{name: "error no error", initial: Error[int](fakeError), mapping: toString(nil), expect: Error[string](fakeError)},
		{name: "error error", initial: Error[int](fakeError), mapping: toString(fakeError2), expect: Error[string](fakeError)},
		{name: "empty no error", initial: Empty[int](), mapping: toString(nil), expect: Empty[string]()},
		{name: "empty present", initial: Empty[int](), mapping: toString(fakeError2), expect: Empty[string]()},
		{name: "value no error", initial: Value(42), mapping: toString(nil), expect: Value("42")},
		{name: "value error", initial: Value(42), mapping: toString(fakeError2), expect: Error[string](fakeError2)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := MapResultUnsafe(tt.initial, tt.mapping); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("MapResultUnsafe() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestFlatMap(t *testing.T) {
	toString := func(value int) Result[string] {
		return Value(fmt.Sprint(value))
	}

	tests := []struct {
		name    string
		initial Result[int]
		expect  Result[string]
	}{
		{name: "error", initial: Error[int](fakeError), expect: Error[string](fakeError)},
		{name: "empty", initial: Empty[int](), expect: Empty[string]()},
		{name: "value", initial: Value(42), expect: Value("42")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := FlatMapResult(tt.initial, toString); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("FlatMapResult() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestFilter(t *testing.T) {
	filter := func(accept bool) func(value int) bool {
		return func(value int) bool {
			return accept
		}
	}

	tests := []struct {
		name    string
		initial Result[int]
		filter  func(int) bool
		expect  Result[int]
	}{
		{name: "error accept", initial: Error[int](fakeError), filter: filter(true), expect: Error[int](fakeError)},
		{name: "error does not accept", initial: Error[int](fakeError), filter: filter(false), expect: Error[int](fakeError)},
		{name: "empty accept", initial: Empty[int](), filter: filter(true), expect: Empty[int]()},
		{name: "empty does not accept", initial: Empty[int](), filter: filter(false), expect: Empty[int]()},
		{name: "value accept", initial: Value(42), filter: filter(true), expect: Value(42)},
		{name: "value does not accept", initial: Value(42), filter: filter(false), expect: Empty[int]()},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.Filter(tt.filter); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("Filter() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestExecute(t *testing.T) {
	intPtr := func(value int) *int {
		return &value
	}

	Double := func(value *int) {
		*value *= 2
	}

	tests := []struct {
		name    string
		initial Result[*int]
		expect  Result[*int]
	}{
		{name: "error", initial: Error[*int](fakeError), expect: Error[*int](fakeError)},
		{name: "empty", initial: Empty[*int](), expect: Empty[*int]()},
		{name: "value", initial: Value(intPtr(42)), expect: Value(intPtr(84))},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.Execute(Double); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("ExecuteOrCrash() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrMap(t *testing.T) {
	handle := func(value int, err error) int {
		return 99
	}
	tests := []struct {
		name    string
		initial Result[int]
		expect  Result[int]
	}{
		{name: "error", initial: Error[int](fakeError), expect: Value[int](99)},
		{name: "empty", initial: Empty[int](), expect: Value[int](99)},
		{name: "value", initial: Value(42), expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrMap(handle); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrHandleError() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrValue(t *testing.T) {

	tests := []struct {
		name    string
		initial Result[int]
		expect  Result[int]
	}{
		{name: "error", initial: Error[int](fakeError), expect: Value[int](99)},
		{name: "empty", initial: Empty[int](), expect: Value[int](99)},
		{name: "value", initial: Value(42), expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrValue(99); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrValue() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrValueFn(t *testing.T) {

	tests := []struct {
		name    string
		initial Result[int]
		expect  Result[int]
	}{
		{name: "error", initial: Error[int](fakeError), expect: Value[int](99)},
		{name: "empty", initial: Empty[int](), expect: Value[int](99)},
		{name: "value", initial: Value(42), expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrValueFn(func() int { return 99 }); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrValueFn() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrOptionalValue(t *testing.T) {

	tests := []struct {
		name    string
		initial Result[int]
		present bool
		expect  Result[int]
	}{
		{name: "error absent", initial: Error[int](fakeError), present: false, expect: Empty[int]()},
		{name: "error present", initial: Error[int](fakeError), present: true, expect: Value[int](99)},
		{name: "empty absent", initial: Empty[int](), present: false, expect: Empty[int]()},
		{name: "empty present", initial: Empty[int](), present: true, expect: Value[int](99)},
		{name: "value absent", initial: Value(42), present: false, expect: Value(42)},
		{name: "value present", initial: Value(42), present: true, expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrOptionalValue(99, tt.present); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrOptionalValue() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrOptionalValueFn(t *testing.T) {

	tests := []struct {
		name    string
		initial Result[int]
		present bool
		expect  Result[int]
	}{
		{name: "error absent", initial: Error[int](fakeError), present: false, expect: Empty[int]()},
		{name: "error present", initial: Error[int](fakeError), present: true, expect: Value[int](99)},
		{name: "empty absent", initial: Empty[int](), present: false, expect: Empty[int]()},
		{name: "empty present", initial: Empty[int](), present: true, expect: Value[int](99)},
		{name: "value absent", initial: Value(42), present: false, expect: Value(42)},
		{name: "value present", initial: Value(42), present: true, expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrOptionalValueFn(func() (int, bool) { return 99, tt.present }); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrOptionalValueFn() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrUnsafeValue(t *testing.T) {

	tests := []struct {
		name    string
		initial Result[int]
		err     error
		expect  Result[int]
	}{
		{name: "error error", initial: Error[int](fakeError), err: fakeError2, expect: ErrorValue[int](99, fakeError2)},
		{name: "error no error", initial: Error[int](fakeError), err: nil, expect: Value[int](99)},
		{name: "empty error", initial: Empty[int](), err: fakeError2, expect: ErrorValue[int](99, fakeError2)},
		{name: "empty no error", initial: Empty[int](), err: nil, expect: Value[int](99)},
		{name: "value error", initial: Value(42), err: fakeError2, expect: Value(42)},
		{name: "value no error", initial: Value(42), err: nil, expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrUnsafeValue(99, tt.err); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrUnsafeValue() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestOrUnsafeValueFn(t *testing.T) {

	tests := []struct {
		name    string
		initial Result[int]
		err     error
		expect  Result[int]
	}{
		{name: "error error", initial: Error[int](fakeError), err: fakeError2, expect: ErrorValue[int](99, fakeError2)},
		{name: "error no error", initial: Error[int](fakeError), err: nil, expect: Value[int](99)},
		{name: "empty error", initial: Empty[int](), err: fakeError2, expect: ErrorValue[int](99, fakeError2)},
		{name: "empty no error", initial: Empty[int](), err: nil, expect: Value[int](99)},
		{name: "value error", initial: Value(42), err: fakeError2, expect: Value(42)},
		{name: "value no error", initial: Value(42), err: nil, expect: Value(42)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if result := tt.initial.OrUnsafeValueFn(func() (int, error) { return 99, tt.err }); !reflect.DeepEqual(result, tt.expect) {
				t.Errorf("OrUnsafeValueFn() result = %v, expected %v", result, tt.expect)
			}
		})
	}
}

func TestError(t *testing.T) {
	tests := []struct {
		name    string
		initial Result[string]
		expect  error
	}{
		{name: "error", initial: Error[string](fakeError), expect: fakeError},
		{name: "empty", initial: Empty[string](), expect: nil},
		{name: "value", initial: Value("fake value"), expect: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.initial.Error(); err != tt.expect {
				t.Errorf("Error() error = %v, expected %v", err, tt.expect)
			}
		})
	}
}

func TestValue(t *testing.T) {
	tests := []struct {
		name    string
		initial Result[string]
		expect  string
	}{
		{name: "error", initial: Error[string](fakeError), expect: ""},
		{name: "empty", initial: Empty[string](), expect: ""},
		{name: "value", initial: Value("fake value"), expect: "fake value"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if value := tt.initial.Value(); value != tt.expect {
				t.Errorf("Value() value = %v, expected %v", value, tt.expect)
			}
		})
	}
}

func TestOptionalValue(t *testing.T) {
	tests := []struct {
		name    string
		initial Result[string]
		expect  string
		present bool
	}{
		{name: "error", initial: Error[string](fakeError), expect: "", present: false},
		{name: "empty", initial: Empty[string](), expect: "", present: false},
		{name: "value", initial: Value("fake value"), expect: "fake value", present: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if value, present := tt.initial.OptionalValue(); value != tt.expect || present != tt.present {
				t.Errorf("OptionalResult() value, present = %v, %v, expected %v, %v", value, present, tt.expect, tt.present)
			}
		})
	}
}

func TestUnsafeValue(t *testing.T) {
	tests := []struct {
		name    string
		initial Result[string]
		expect  string
		err     error
	}{
		{name: "error", initial: Error[string](fakeError), expect: "", err: fakeError},
		{name: "empty", initial: Empty[string](), expect: "", err: NoSuchValueError},
		{name: "value", initial: Value("fake value"), expect: "fake value", err: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if value, err := tt.initial.UnsafeValue(); value != tt.expect || err != tt.err {
				t.Errorf("OptionalResult() value, present = %v, %v, expected %v, %v", value, err, tt.expect, tt.err)
			}
		})
	}
}

func TestWrapErrorFn(t *testing.T) {

	newError := fmt.Errorf("%s", "new error")
	conversion := func(err error) error {
		return newError
	}

	tests := []struct {
		name    string
		initial Result[string]
		value   string
		err     error
	}{
		{name: "error", initial: Error[string](fakeError), value: "", err: newError},
		{name: "empty", initial: Empty[string](), value: "", err: NoSuchValueError},
		{name: "value", initial: Value("value"), value: "value", err: nil},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if value, err := tt.initial.WrapErrorFn(conversion).UnsafeValue(); value != tt.value || err != tt.err {
				t.Errorf("value >> expected %s, received: %s / error >> expected = %v, received %v", tt.value, value, tt.err, err)
			}
		})
	}

}
