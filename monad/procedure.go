package monad

func UnsafeExecution(err error) Execution {
	if err != nil {
		return Failure(err)
	}

	return Success()

}

func Execute(f func()) Execution {
	f()
	return Success()
}

func ExecuteUnsafe(f func() error) Execution {
	return UnsafeExecution(f())
}

type Execution interface {
	Execute(func()) Execution
	ExecuteUnsafe(func() error) Execution
	Flatten(func() Execution) Execution
	OrExecute(func(error)) Execution
	OrExecuteUnsafe(func(error) error) Execution
	OrFlatten(func(error) Execution) Execution
	WrapError(message string) Execution
	WrapErrorFn(func(error) error) Execution
	Error() error
}

func MapExecution[T any](execution Execution, supplier func() T) Result[T] {
	if err := execution.Error(); err != nil {
		return Error[T](err)
	}
	return Value(supplier())
}

func MapExecutionOptional[T any](execution Execution, supplier func() (T, bool)) Result[T] {
	if err := execution.Error(); err != nil {
		return Error[T](err)
	}
	return OptionalResult(supplier())
}

func MapExecutionUnsafe[T any](execution Execution, supplier func() (T, error)) Result[T] {
	if err := execution.Error(); err != nil {
		return Error[T](err)
	}
	return UnsafeResult(supplier())
}

func UpgradeExecution[T any](execution Execution, supplier func() Result[T]) Result[T] {
	if err := execution.Error(); err != nil {
		return Error[T](err)
	}
	return supplier()
}
