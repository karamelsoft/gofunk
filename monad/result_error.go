package monad

import "fmt"

func Error[T any](err error) Result[T] {
	return &ErrorResult[T]{
		err: err,
	}
}

func ErrorValue[T any](value T, err error) Result[T] {
	return &ErrorResult[T]{
		value: value,
		err:   err,
	}
}

type ErrorResult[T any] struct {
	value T
	err   error
}

func (e *ErrorResult[T]) Map(_ func(T) T) Result[T] {
	return e
}

func (e *ErrorResult[T]) MapOptional(_ func(T) (T, bool)) Result[T] {
	return e
}

func (e *ErrorResult[T]) MapUnsafe(_ func(T) (T, error)) Result[T] {
	return e
}

func (e *ErrorResult[T]) FlatMap(_ func(T) Result[T]) Result[T] {
	return e
}

func (e *ErrorResult[T]) Filter(_ func(T) bool) Result[T] {
	return e
}

func (e *ErrorResult[T]) Execute(_ func(T)) Result[T] {
	return e
}

func (e *ErrorResult[T]) ExecuteUnsafe(_ func(T) error) Result[T] {
	return e
}

func (e *ErrorResult[T]) OrValue(value T) Result[T] {
	return Value(value)
}

func (e *ErrorResult[T]) OrValueFn(supplier func() T) Result[T] {
	return ResultFn(supplier)
}

func (e *ErrorResult[T]) OrOptionalValue(value T, present bool) Result[T] {
	return OptionalResult(value, present)
}

func (e *ErrorResult[T]) OrOptionalValueFn(supplier func() (T, bool)) Result[T] {
	return OptionalResultFn(supplier)
}

func (e *ErrorResult[T]) OrUnsafeValue(value T, err error) Result[T] {
	return UnsafeResult(value, err)
}

func (e *ErrorResult[T]) OrUnsafeValueFn(supplier func() (T, error)) Result[T] {
	return UnsafeResultFn(supplier)
}

func (e *ErrorResult[T]) OrMap(operation func(T, error) T) Result[T] {
	return Value(operation(e.value, e.err))
}

func (e *ErrorResult[T]) OrMapOptional(operation func(T, error) (T, bool)) Result[T] {
	return OptionalResult(operation(e.value, e.err))
}

func (e *ErrorResult[T]) OrMapUnsafe(operation func(T, error) (T, error)) Result[T] {
	return UnsafeResult(operation(e.value, e.err))
}

func (e *ErrorResult[T]) OrExecute(operation func(T, error)) Result[T] {
	operation(e.value, e.err)
	return e
}

func (e *ErrorResult[T]) OrExecuteUnsafe(operation func(T, error) error) Result[T] {
	if err := operation(e.value, e.err); err != nil {
		return Error[T](err)
	}
	return e
}

func (e *ErrorResult[T]) WrapError(message string) Result[T] {
	return ErrorValue(e.value, fmt.Errorf("%s: %w", message, e.err))
}

func (e *ErrorResult[T]) WrapErrorFn(operation func(error) error) Result[T] {
	return ErrorValue(e.value, operation(e.err))
}

func (e *ErrorResult[T]) Value() T {
	return e.value
}

func (e *ErrorResult[T]) Error() error {
	return e.err
}

func (e *ErrorResult[T]) OptionalValue() (T, bool) {
	return e.value, false
}

func (e *ErrorResult[T]) UnsafeValue() (T, error) {
	return e.value, e.err
}
