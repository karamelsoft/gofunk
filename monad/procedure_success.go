package monad

func Success() Execution {
	return Succeeded{}
}

type Succeeded struct {
}

func (p Succeeded) Execute(f func()) Execution {
	f()
	return p
}

func (p Succeeded) ExecuteUnsafe(f func() error) Execution {
	return ExecuteUnsafe(f)
}

func (p Succeeded) Flatten(f func() Execution) Execution {
	return f()
}

func (p Succeeded) OrExecute(f func(error)) Execution {
	return p
}

func (p Succeeded) OrExecuteUnsafe(f func(error) error) Execution {
	return p
}

func (p Succeeded) OrFlatten(f func(error) Execution) Execution {
	return p
}

func (p Succeeded) WrapError(_ string) Execution {
	return p
}

func (p Succeeded) Error() error {
	return nil
}

func (p Succeeded) WrapErrorFn(_ func(error) error) Execution {
	return p
}
