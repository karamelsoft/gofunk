package monad

func ResultFn[T any](supplier func() T) Result[T] {
	return Value(supplier())
}

func OptionalResult[T any](value T, present bool) Result[T] {
	if present {
		return Value[T](value)
	}
	return Empty[T]()
}

func OptionalResultFn[T any](supplier func() (T, bool)) Result[T] {
	return OptionalResult(supplier())
}

func UnsafeResult[T any](value T, err error) Result[T] {
	if err != nil {
		return ErrorValue[T](value, err)
	}
	return Value[T](value)
}

func UnsafeResultFn[T any](supplier func() (value T, err error)) Result[T] {
	return UnsafeResult(supplier())
}

type Result[T any] interface {
	Execute(func(T)) Result[T]
	ExecuteUnsafe(func(T) error) Result[T]
	Map(func(T) T) Result[T]
	MapOptional(func(T) (T, bool)) Result[T]
	MapUnsafe(func(T) (T, error)) Result[T]
	FlatMap(func(T) Result[T]) Result[T]
	Filter(func(T) bool) Result[T]
	OrValue(T) Result[T]
	OrValueFn(func() T) Result[T]
	OrOptionalValue(T, bool) Result[T]
	OrOptionalValueFn(func() (T, bool)) Result[T]
	OrUnsafeValue(T, error) Result[T]
	OrUnsafeValueFn(func() (T, error)) Result[T]
	OrMap(func(T, error) T) Result[T]
	OrMapOptional(func(T, error) (T, bool)) Result[T]
	OrMapUnsafe(func(T, error) (T, error)) Result[T]
	OrExecute(func(T, error)) Result[T]
	OrExecuteUnsafe(func(T, error) error) Result[T]
	WrapError(message string) Result[T]
	WrapErrorFn(func(error) error) Result[T]
	Value() T
	Error() error
	OptionalValue() (T, bool)
	UnsafeValue() (T, error)
}

func MapResult[T any, U any](result Result[T], mapping func(T) U) Result[U] {
	if err := result.Error(); err != nil {
		return Error[U](err)
	}
	if value, present := result.OptionalValue(); present {
		return Value[U](mapping(value))
	}
	return Empty[U]()
}

func MapResultOptional[T any, U any](result Result[T], mapping func(T) (U, bool)) Result[U] {
	if err := result.Error(); err != nil {
		return Error[U](err)
	}
	if value, present := result.OptionalValue(); present {
		return OptionalResult[U](mapping(value))
	}
	return Empty[U]()
}

func MapResultUnsafe[T any, U any](result Result[T], mapping func(T) (U, error)) Result[U] {
	if err := result.Error(); err != nil {
		return Error[U](err)
	}
	if value, present := result.OptionalValue(); present {
		return UnsafeResult[U](mapping(value))
	}
	return Empty[U]()
}

func FlatMapResult[T any, U any](result Result[T], mapping func(T) Result[U]) Result[U] {
	if err := result.Error(); err != nil {
		return Error[U](err)
	}
	if value, present := result.OptionalValue(); present {
		return mapping(value)
	}
	return Empty[U]()
}

func AggregateResults[T, U, V any](result1 Result[T], result2 Result[U], mapping func(T, U) Result[V]) Result[V] {
	return FlatMapResult[T, V](result1, func(value1 T) Result[V] {
		return FlatMapResult[U, V](result2, func(value2 U) Result[V] {
			return mapping(value1, value2)
		})
	})
}

func BiExecuteUnsafe[T, U any](result1 Result[T], result2 Result[U], mapping func(T, U) error) Execution {
	return UnsafeExecution(
		result1.ExecuteUnsafe(func(value1 T) error {
			return result2.ExecuteUnsafe(func(value2 U) error {
				return mapping(value1, value2)
			}).Error()
		}).Error(),
	)
}
