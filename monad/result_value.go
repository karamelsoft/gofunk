package monad

func Value[T any](value T) Result[T] {
	return &ValueResult[T]{
		value: value,
	}
}

type ValueResult[T any] struct {
	value T
}

func (v *ValueResult[T]) Map(function func(T) T) Result[T] {
	return Value[T](function(v.value))
}

func (v *ValueResult[T]) MapOptional(function func(T) (T, bool)) Result[T] {
	if value, present := function(v.value); present {
		return Value[T](value)
	}
	return Empty[T]()
}

func (v *ValueResult[T]) MapUnsafe(function func(T) (T, error)) Result[T] {
	value, err := function(v.value)
	if err != nil {
		return Error[T](err)
	}
	return Value[T](value)
}

func (v *ValueResult[T]) FlatMap(function func(T) Result[T]) Result[T] {
	return function(v.value)
}

func (v *ValueResult[T]) Filter(filter func(T) bool) Result[T] {
	if filter(v.value) {
		return v
	}
	return Empty[T]()
}

func (v *ValueResult[T]) Execute(procedure func(T)) Result[T] {
	procedure(v.value)
	return v
}

func (v *ValueResult[T]) ExecuteUnsafe(procedure func(T) error) Result[T] {
	if err := procedure(v.value); err != nil {
		return ErrorValue(v.value, err)
	}
	return v
}

func (v *ValueResult[T]) OrValue(_ T) Result[T] {
	return v
}

func (v *ValueResult[T]) OrValueFn(_ func() T) Result[T] {
	return v
}

func (v *ValueResult[T]) OrOptionalValue(_ T, _ bool) Result[T] {
	return v
}

func (v *ValueResult[T]) OrOptionalValueFn(_ func() (T, bool)) Result[T] {
	return v
}

func (v *ValueResult[T]) OrUnsafeValue(_ T, _ error) Result[T] {
	return v
}

func (v *ValueResult[T]) OrUnsafeValueFn(_ func() (T, error)) Result[T] {
	return v
}

func (v *ValueResult[T]) OrMap(_ func(T, error) T) Result[T] {
	return v
}

func (v *ValueResult[T]) OrMapOptional(_ func(T, error) (T, bool)) Result[T] {
	return v
}

func (v *ValueResult[T]) OrMapUnsafe(_ func(T, error) (T, error)) Result[T] {
	return v
}

func (v *ValueResult[T]) OrExecute(_ func(T, error)) Result[T] {
	return v
}

func (v *ValueResult[T]) OrExecuteUnsafe(_ func(T, error) error) Result[T] {
	return v
}

func (v *ValueResult[T]) WrapError(_ string) Result[T] {
	return v
}

func (v *ValueResult[T]) WrapErrorFn(_ func(error) error) Result[T] {
	return v
}

func (v *ValueResult[T]) Value() T {
	return v.value
}

func (v *ValueResult[T]) Error() error {
	return nil
}

func (v *ValueResult[T]) OptionalValue() (T, bool) {
	return v.value, true
}

func (v *ValueResult[T]) UnsafeValue() (T, error) {
	return v.value, nil
}
