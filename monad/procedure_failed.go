package monad

import "fmt"

func Failure(err error) Execution {
	return &Failed{
		err: err,
	}
}

type Failed struct {
	err error
}

func (p *Failed) Execute(f func()) Execution {
	return p
}

func (p *Failed) ExecuteUnsafe(f func() error) Execution {
	return p
}

func (p *Failed) Flatten(f func() Execution) Execution {
	return p
}

func (p *Failed) OrExecute(f func(error)) Execution {
	return Execute(func() {
		f(p.err)
	})
}

func (p *Failed) OrExecuteUnsafe(f func(error) error) Execution {
	return ExecuteUnsafe(func() error {
		return f(p.err)
	})
}

func (p *Failed) OrFlatten(f func(error) Execution) Execution {
	return f(p.err)
}

func (p *Failed) WrapError(message string) Execution {
	return Failure(fmt.Errorf("%s: %w", message, p.err))
}

func (p *Failed) Error() error {
	return p.err
}

func (p *Failed) WrapErrorFn(operation func(error) error) Execution {
	return Failure(operation(p.err))
}
