package monad

import "fmt"

var (
	NoSuchValueError = fmt.Errorf("no value")
)

func Empty[T any]() Result[T] {
	return &EmptyResult[T]{}
}

type EmptyResult[T any] struct {
	value T
}

func (e *EmptyResult[T]) Map(_ func(T) T) Result[T] {
	return e
}

func (e *EmptyResult[T]) MapOptional(_ func(T) (T, bool)) Result[T] {
	return e
}

func (e *EmptyResult[T]) MapUnsafe(_ func(T) (T, error)) Result[T] {
	return e
}

func (e *EmptyResult[T]) FlatMap(_ func(T) Result[T]) Result[T] {
	return e
}

func (e *EmptyResult[T]) Filter(func(T) bool) Result[T] {
	return e
}

func (e *EmptyResult[T]) Execute(_ func(T)) Result[T] {
	return e
}

func (e *EmptyResult[T]) ExecuteUnsafe(_ func(T) error) Result[T] {
	return e
}

func (e *EmptyResult[T]) OrValue(value T) Result[T] {
	return Value[T](value)
}

func (e *EmptyResult[T]) OrValueFn(provider func() T) Result[T] {
	return ResultFn(provider)
}

func (e *EmptyResult[T]) OrOptionalValue(value T, present bool) Result[T] {
	return OptionalResult(value, present)
}

func (e *EmptyResult[T]) OrOptionalValueFn(provider func() (T, bool)) Result[T] {
	return OptionalResultFn(provider)
}

func (e *EmptyResult[T]) OrUnsafeValue(value T, err error) Result[T] {
	return UnsafeResult(value, err)
}

func (e *EmptyResult[T]) OrUnsafeValueFn(provider func() (T, error)) Result[T] {
	return UnsafeResultFn(provider)
}

func (e *EmptyResult[T]) OrMap(operation func(T, error) T) Result[T] {
	return Value(operation(e.value, nil))
}

func (e *EmptyResult[T]) OrMapOptional(operation func(T, error) (T, bool)) Result[T] {
	return OptionalResult(operation(e.value, nil))
}

func (e *EmptyResult[T]) OrMapUnsafe(operation func(T, error) (T, error)) Result[T] {
	return UnsafeResult(operation(e.value, nil))
}

func (e *EmptyResult[T]) OrExecute(operation func(T, error)) Result[T] {
	operation(e.value, nil)
	return e
}

func (e *EmptyResult[T]) OrExecuteUnsafe(operation func(T, error) error) Result[T] {
	if err := operation(e.value, nil); err != nil {
		return Error[T](err)
	}
	return e
}

func (e *EmptyResult[T]) WrapError(_ string) Result[T] {
	return e
}

func (e *EmptyResult[T]) WrapErrorFn(_ func(error) error) Result[T] {
	return e
}

func (e *EmptyResult[T]) Value() T {
	return e.value
}

func (e *EmptyResult[T]) Error() error {
	return nil
}

func (e *EmptyResult[T]) OptionalValue() (T, bool) {
	return e.value, false
}

func (e *EmptyResult[T]) UnsafeValue() (T, error) {
	return e.value, NoSuchValueError
}
