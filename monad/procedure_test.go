package monad

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	fakeError  = fmt.Errorf("fake error")
	fakeError2 = fmt.Errorf("fake error: %w", fakeError)
	message    = "fake message"
)

func TestProcedure_Then(t *testing.T) {
	tests := []struct {
		name      string
		initial   Execution
		expect    Execution
		expectMsg string
	}{
		{name: "success", initial: Success(), expect: Success(), expectMsg: message},
		{name: "failure", initial: Failure(fakeError), expect: Failure(fakeError), expectMsg: ""},
	}
	for _, tt := range tests {
		var destination string
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.Execute(setMessage(&destination)))
			assert.Equal(t, tt.expectMsg, destination)
		})
	}
}

func TestProcedure_ThenUnsafe(t *testing.T) {
	var destination string
	tests := []struct {
		name      string
		initial   Execution
		expect    Execution
		operation func() error
		expectMsg string
	}{
		{name: "success_ok", initial: Success(), expect: Success(), operation: setMessageOk(&destination), expectMsg: message},
		{name: "success_ko", initial: Success(), expect: Failure(fakeError), operation: setMessageFailed(), expectMsg: ""},
		{name: "failure_ok", initial: Failure(fakeError), expect: Failure(fakeError), operation: setMessageOk(&destination), expectMsg: ""},
		{name: "failure_ko", initial: Failure(fakeError), expect: Failure(fakeError), operation: setMessageFailed(), expectMsg: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.ExecuteUnsafe(tt.operation))
			assert.Equal(t, tt.expectMsg, destination)
			destination = ""
		})
	}
}

func TestProcedure_ThenFlatten(t *testing.T) {
	var destination string
	tests := []struct {
		name      string
		initial   Execution
		expect    Execution
		operation func() Execution
		expectMsg string
	}{
		{name: "success_ok", initial: Success(), expect: Success(), operation: wrap(setMessageOk(&destination)), expectMsg: message},
		{name: "success_ko", initial: Success(), expect: Failure(fakeError), operation: wrap(setMessageFailed()), expectMsg: ""},
		{name: "failure_ok", initial: Failure(fakeError), expect: Failure(fakeError), operation: wrap(setMessageOk(&destination)), expectMsg: ""},
		{name: "failure_ko", initial: Failure(fakeError), expect: Failure(fakeError), operation: wrap(setMessageFailed()), expectMsg: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.Flatten(tt.operation))
			assert.Equal(t, tt.expectMsg, destination)
			destination = ""
		})
	}
}

func TestProcedure_Or(t *testing.T) {
	tests := []struct {
		name      string
		initial   Execution
		expect    Execution
		expectMsg string
	}{
		{name: "success", initial: Success(), expect: Success(), expectMsg: ""},
		{name: "failure", initial: Failure(fakeError), expect: Success(), expectMsg: fakeError.Error()},
	}
	for _, tt := range tests {
		var destination string
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.OrExecute(orSetMessage(&destination)))
			assert.Equal(t, tt.expectMsg, destination)
		})
	}
}

func TestProcedure_OrUnsafe(t *testing.T) {
	var destination string
	tests := []struct {
		name      string
		initial   Execution
		expect    Execution
		operation func(error) error
		expectMsg string
	}{
		{name: "success_ok", initial: Success(), expect: Success(), operation: orSetMessageOk(&destination), expectMsg: ""},
		{name: "success_ko", initial: Success(), expect: Success(), operation: orSetMessageFailed(), expectMsg: ""},
		{name: "failure_ok", initial: Failure(fakeError), expect: Success(), operation: orSetMessageOk(&destination), expectMsg: fakeError.Error()},
		{name: "failure_ko", initial: Failure(fakeError), expect: Failure(fakeError), operation: orSetMessageFailed(), expectMsg: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.OrExecuteUnsafe(tt.operation))
			assert.Equal(t, tt.expectMsg, destination)
			destination = ""
		})
	}
}

func TestProcedure_OrFlatten(t *testing.T) {
	var destination string
	tests := []struct {
		name      string
		initial   Execution
		expect    Execution
		operation func(error) Execution
		expectMsg string
	}{
		{name: "success_ok", initial: Success(), expect: Success(), operation: wrapErr(orSetMessageOk(&destination)), expectMsg: ""},
		{name: "success_ko", initial: Success(), expect: Success(), operation: wrapErr(orSetMessageFailed()), expectMsg: ""},
		{name: "failure_ok", initial: Failure(fakeError), expect: Success(), operation: wrapErr(orSetMessageOk(&destination)), expectMsg: fakeError.Error()},
		{name: "failure_ko", initial: Failure(fakeError), expect: Failure(fakeError), operation: wrapErr(orSetMessageFailed()), expectMsg: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.OrFlatten(tt.operation))
			assert.Equal(t, tt.expectMsg, destination)
			destination = ""
		})
	}
}

func TestProcedure_WrapErrorFn(t *testing.T) {

	newError := errors.New("new error")
	wrap := func(err error) error {
		return newError
	}

	tests := []struct {
		name    string
		initial Execution
		expect  Execution
		err     error
	}{
		{name: "success", initial: Success(), expect: Success(), err: nil},
		{name: "failure", initial: Failure(fakeError), expect: Failure(newError), err: newError},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expect, tt.initial.WrapErrorFn(wrap))
			assert.Equal(t, tt.err, tt.initial.WrapErrorFn(wrap).Error())
		})
	}
}

func setMessage(destination *string) func() {
	return func() {
		*destination = message
	}
}

func orSetMessage(destination *string) func(error) {
	return func(err error) {
		*destination = err.Error()
	}
}

func setMessageOk(destination *string) func() error {
	return func() error {
		*destination = message
		return nil
	}
}

func orSetMessageOk(destination *string) func(error) error {
	return func(err error) error {
		*destination = err.Error()
		return nil
	}
}

func setMessageFailed() func() error {
	return func() error {
		return fakeError
	}
}

func orSetMessageFailed() func(error) error {
	return func(err error) error {
		return fakeError
	}
}

func wrap(operation func() error) func() Execution {
	return func() Execution {
		return ExecuteUnsafe(operation)
	}
}

func wrapErr(operation func(error) error) func(error) Execution {
	return func(err error) Execution {
		return UnsafeExecution(operation(err))
	}
}
