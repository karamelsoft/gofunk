# Gofunk
## Introduction

Go-Funk has been designed by and for developers who love Golang but come from different rich languages
which have a lot more capabilities that could be missing like:

* Monads
* Streams
* Array Functions

If you do not miss any functional features from Golang, this project is useless for you.

## How to get gofunk

```bash
go get gitlab.com/karamelsoft/gofunk
```

## How to use gofunk

This is a guideline of all the features of gofunk

### Slices

Operations on slices:
1. Map
2. Filer
3. First [Optional | Unsafe]

### Streams

Operations on streams:
1. ForEach
2. Filter
3. Map [Optional | Unsafe]
4. FlatMap [Optional | Unsafe]
5. Fold

###Monads

#### Executions

Operations on executions:
1. Execute [Unsafe]
2. Flatten
3. OrExecute [Unsafe]
4. OrFlatten

#### Results

Operations on results:
1. Execute [Unsafe]
2. Flatten
3. OrExecute [Unsafe]
4. OrFlatten

